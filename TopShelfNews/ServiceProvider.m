//
//  ServiceProvider.m
//  TopShelfNews
//
//  Created by Markus Stöbe on 27.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "ServiceProvider.h"

@interface ServiceProvider ()

@property (nonatomic, strong) RssParser      *rssparser;   //an instance of our RssParser that loads one feed and parses all entries
@property (nonatomic, strong) TVContentItem  *wrapperItem;
@end

@implementation ServiceProvider


- (instancetype)init {
    self = [super init];
    if (self) {
        self.rssparser = [[RssParser alloc] initWithDelegate:self];
        
        //we create one content-item to hold all others. this will be the only sction in the topshelf
        //we could do without but then we must select another display style and can't get square preview images
        TVContentIdentifier *wrapperID = [[TVContentIdentifier alloc] initWithIdentifier:@"folderItem" container:nil];
        self.wrapperItem               = [[TVContentItem alloc] initWithContentIdentifier:wrapperID];
        self.wrapperItem.title         = @"Mac & i News";
    }
    return self;
}

#pragma mark - TVTopShelfProvider protocol

- (TVTopShelfContentStyle)topShelfStyle {
    // Return desired Top Shelf style.
    return TVTopShelfContentStyleSectioned;
}

- (NSArray *)topShelfItems {
    //Reload and parse the rss-feed every time tvOS asks for the list of topshelf-items
    [self.rssparser loadNews];
    
    //return an array of topshelf-items. we return an array with our wrapper-object that itself contains one object per story
    return @[self.wrapperItem];
}

#pragma mark - RssParser protocol
- (void)RssParser:(RssParser*)thisParser didFinishParsingAndFoundNews:(NSArray*)foundStories {
    //temporary array to collect all the story-items
    NSMutableArray *entries = [NSMutableArray new];

    //a counter to build an ID for every item
    //we use an integer so this will correlate to the entries position in the tableview taht is presented in the app
    for (NewsItem *thisStory in foundStories) {
        //make an id for each entry
        TVContentIdentifier *thisID = [[TVContentIdentifier alloc] initWithIdentifier:thisStory.link
                                                                            container:self.wrapperItem.contentIdentifier];
        //create a fresh contentItem to hold the data
        TVContentItem     *thisItem = [[TVContentItem alloc] initWithContentIdentifier:thisID];

        //set imageURL,title and textURL
        CGSize optSize = TVTopShelfImageSizeForShape(TVContentItemImageShapeExtraWide, TVTopShelfContentStyleInset);
        NSString *url  = thisStory.imageURL;
        
        //get high-res images from backend by setting maximum size and highest quality (q100)in url
        url = [url stringByReplacingOccurrencesOfString:@"/200/q50/" withString:[NSString stringWithFormat:@"/%0.f/q100/", optSize.width]];

        thisItem.imageURL   = [NSURL URLWithString:url];
        thisItem.imageShape = TVContentItemImageShapeHDTV;

        NSURLComponents *tempURL = [NSURLComponents componentsWithString:thisStory.link];
        tempURL.scheme = @"heise";

        thisItem.displayURL = tempURL.URL;
        thisItem.playURL    = tempURL.URL;
        thisItem.title      = thisStory.title;
        
        //add item to our temp-array
        [entries addObject:thisItem];
    }
    
    //if all items are created and collected, add them as childs to our wrapper-item
    self.wrapperItem.topShelfItems = entries;

    //let tvOS know that we got some new images
    [[NSNotificationCenter defaultCenter] postNotificationName:TVTopShelfItemsDidChangeNotification object:nil];
}


@end
