//
//  DetailViewController.h
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsItem.h"
#import "HTMLReader.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NewsItem *detailItem;
@property (weak, nonatomic) IBOutlet UITextView *detailText;
@property (weak, nonatomic) IBOutlet UIImageView *detailImage;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailImageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailImageWidth;


@end
