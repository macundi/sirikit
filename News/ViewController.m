//
//  ViewController.m
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) RssParser *rssparser;   //an instance of our RssParser that loads one feed and parses all entries
@property (nonatomic, strong) NSArray   *stories;     //array holding a list of dicitionaries, each one representing a story

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.stories   = [NSArray new];
    self.rssparser = [[RssParser alloc] initWithDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //loading the news must happen everytime the view will be presented, otherwise jumping to a story from topShelf will not always work
    //(if topShelf has a new story that the app hasn't loaded yet because it's been in the background sleeping, for example)
    [self.rssparser loadNews];
}

- (void)RssParser:(RssParser*)thisParser didFinishParsingAndFoundNews:(NSArray*)foundStories {
    NSLog(@"I found %ld stories at Mac & i", foundStories.count);
    self.stories = [foundStories copy];
    [self.tableView reloadData];
}

//******************************************************************************************************************
#pragma mark - Table View
//******************************************************************************************************************
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.stories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NewsItem *newsItem        = self.stories[indexPath.row];
    cell.textLabel.text       = newsItem.title;
    cell.detailTextLabel.text = newsItem.date;
    return cell;
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator {
    
    UITableViewCell *previousCell = (UITableViewCell*)context.previouslyFocusedView;
    UITableViewCell *nextCell     = (UITableViewCell*)context.nextFocusedView;
    
    [coordinator addCoordinatedAnimations: ^{
        NSTimeInterval duration = [UIView inheritedAnimationDuration];
        
        if(previousCell && nextCell) {
            [UIView animateWithDuration:(4*duration)    //duration*4 is just a gimmick, because otherwise you wouldn't notice the animations!
                                  delay:0.0
                                options:UIViewAnimationOptionOverrideInheritedDuration
                             animations: ^{
                                 previousCell.backgroundColor = [UIColor whiteColor];
                                 previousCell.transform       = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                                 nextCell.backgroundColor = [UIColor colorWithWhite:0.97 alpha:0.9];
                                 nextCell.transform       = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
                             } completion:nil];
        }
    } completion:nil];

}

//******************************************************************************************************************
#pragma mark - Segues
//******************************************************************************************************************
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DetailViewController *controller = (DetailViewController *)[segue destinationViewController];

    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NewsItem *newsItem     = self.stories[indexPath.row];
        controller.detailItem  = newsItem;
    }
    
    else if ([[segue identifier] isEqualToString:@"showPreselectedStory"]) {
        NSString *selectedStoryURL = (NSString *)sender;
        if (selectedStoryURL.length > 0) {
            for (NewsItem *thisStory in self.stories) {
                if ([thisStory.link isEqualToString:selectedStoryURL]) {
                    controller.detailItem = thisStory;
                }
            }
        }
    }
}


@end
