//
//  DetailViewController.m
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (strong, nonatomic) NSString *token;

@end


@implementation DetailViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

#pragma mark - custom setter
- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

#pragma mark - load Details
- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        
        //Define textstyles for intro an paragraphtext
        NSDictionary *introAttr     = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:38.0],
                                        NSForegroundColorAttributeName:[UIColor blackColor]};
        NSDictionary *paragraphAttr = @{NSFontAttributeName:[UIFont systemFontOfSize:36.0],
                                        NSForegroundColorAttributeName:[UIColor darkGrayColor]};
        
        //convert intro from HTML-Text to clean RTF for displaying on screen
        NSString *cleanText = [[[NSAttributedString alloc] initWithData:[self.detailItem.summary dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                          NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                     documentAttributes:nil error:nil] string];
        
        //Set the text
        self.detailText.attributedText = [[NSAttributedString alloc] initWithString:cleanText attributes:introAttr];
        
        //TODO:Move this to a background-thread...or else...
        //load and set the image
        NSString *imageURL   = self.detailItem.imageURL;
        NSURL    *url        = [NSURL URLWithString:imageURL];
        NSData   *imageData  = [NSData dataWithContentsOfURL:url];
        UIImage   *image     = [UIImage imageWithData:imageData];
        self.detailImage.image = image;
        self.backgroundImage.image = image;
        
        //adjust height of imagebox
        float factor = self.detailImageWidth.constant / image.size.width;
        self.detailImageWidth.constant  = image.size.width  * factor;
        self.detailImageHeight.constant = image.size.height * factor;
        
        //get full text from mobile version
        self.detailItem.link = [self.detailItem.link stringByReplacingOccurrencesOfString:@"www." withString:@"m."];
        NSURL *URL = [NSURL URLWithString:self.detailItem.link];
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:URL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
              NSString *contentType = nil;
              if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                  NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
                  contentType = headers[@"Content-Type"];
              }
              
              if (!data) return;
              
              HTMLDocument *home = [HTMLDocument documentWithData:data contentTypeHeader:contentType];
              HTMLElement  *div  = [home firstNodeMatchingSelector:@".meldung_wrapper"];
              HTMLArrayOf(HTMLElement*) *paragraphs = [div nodesMatchingSelector:@"p"];
              
              NSCharacterSet *whitespace = [NSCharacterSet illegalCharacterSet];
              NSAttributedString *intro;
              NSString *article = @"";
              BOOL introWritten = NO;
              
              for (HTMLElement *thisParagraph in paragraphs) {
                  NSString *text = [thisParagraph.textContent stringByTrimmingCharactersInSet:whitespace];
                  
                  NSLog(@"\n\n%@\n\n",text);
                  
                  NSRange badWord = [text rangeOfString:@"Bild"];
                  if (badWord.location != NSNotFound) {
                      continue;
                  }

                  //remove unwanted linebreaks
                  text = [text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
                  
                  //make the first paragraph bold and put it aside
                  if (!introWritten) {
                      intro        = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n",text] attributes:introAttr];
                      introWritten = YES;
                      continue;
                  }
                  
                  //insert 2 linebreaks at the end of each paragraph
                  article = [article stringByAppendingString:[NSString stringWithFormat:@"%@\n\n",text]];
              }
            
            NSMutableAttributedString *finalText = [[NSMutableAttributedString alloc] initWithAttributedString:intro];
            [finalText appendAttributedString:[[NSAttributedString alloc] initWithString:article attributes:paragraphAttr]];
            [self setArticleText:finalText];
          }] resume];
        
        
     }
}

-(void)setArticleText:(NSAttributedString *)newText {
    //since this method is called from a block, make sure we are perfomring updates to the UI from the main-thread
    dispatch_async(dispatch_get_main_queue(), ^{
        self.detailText.attributedText = newText;
    });

}

@end
