//
//  NewsItem.h
//  News
//
//  Created by Markus Stöbe on 28.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *date;

@end
