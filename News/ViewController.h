//
//  ViewController.h
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RssParser.h"
#import "DetailViewController.h"

@interface ViewController : UIViewController<RssDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *logo;

@end

