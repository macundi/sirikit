//
//  IntentHandler.swift
//  News-Siri
//
//  Created by Markus Stöbe on 24.08.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import Intents

class IntentHandler: INExtension, INSearchForPhotosIntentHandling, RssDelegate  {
	var newsItems:Array = [NewsItem]()
	var rssparser: RssParser? = nil
	
	//******************************************************************************************************************
	//* MARK: - INExtension/INIntentHandlerProviding
	//******************************************************************************************************************
    override func handler(for intent: INIntent) -> Any {

		if intent is INSearchForPhotosIntent {
			self.rssparser = RssParser.init(delegate: self)
			self.rssparser?.loadNews()

			let myIntent = intent as! INSearchForPhotosIntent
			print(myIntent.searchTerms)
		}
		return self
    }

	//******************************************************************************************************************
	//* MARK: - INSearchForPhotosIntentHandling
	//******************************************************************************************************************
	func confirm(searchForPhotos intent: INSearchForPhotosIntent, completion: @escaping (INSearchForPhotosIntentResponse) -> Void) {

		//if there are searchterms and news, we might as well do the search
		if intent.searchTerms != nil && self.newsItems.count > 0 {
			//tell Siri we are good to go
			completion(INSearchForPhotosIntentResponse.init(code:.ready, userActivity: nil))
		}

		//no searchterms and/or no news: tell Siri we can't handle the request right now
		completion(INSearchForPhotosIntentResponse.init(code:.failure, userActivity: nil))

	}
	
	func handle(searchForPhotos intent: INSearchForPhotosIntent, completion: @escaping (INSearchForPhotosIntentResponse) -> Void) {
		var allResults = [NewsItem]()
		
		if let terms = intent.searchTerms {
			//if there are searchterms, look for all articles matching them
			allResults = self.findNews(forTerms: terms)
		} else {
			//no searchterms, tell Siri we can't handle the request now
			completion(INSearchForPhotosIntentResponse.init(code:.failure, userActivity: nil))
		}
		
		//collect all imageURLs as keywords for the UserActivity that is send over to the app
		let appInfo = NSUserActivity.init(activityType: "de.macundi.showImages")
		appInfo.keywords = Set(allResults.map({ (item) -> String in item.imageURL }))
		
		//send response to Siri
		completion(INSearchForPhotosIntentResponse.init(code:.continueInApp, userActivity: appInfo))
	}
	
	func findNews(forTerms terms: [String]) -> [NewsItem] {
		var allResults:Array = [NewsItem]()

		for thisTerm in terms {
			let foundItems = newsItems.filter({ (newsItem: NewsItem) -> Bool in
				return newsItem.summary.contains(thisTerm) || newsItem.title.contains(thisTerm)
			})
			
			if foundItems.count > 0 {
				//add results for this term to array of allresults
				print("we found \(foundItems.count) matching articles")
				allResults += foundItems
			}
		}
		
		return allResults
	}
	
	
	//******************************************************************************************************************
	//* MARK: - RssParserDelegate
	//******************************************************************************************************************
	func rssParser(_ parser: RssParser!, didFinishParsingAndFoundNews foundStories: [Any]!) {
		print("I found \(foundStories.count) stories at Mac & i ")
		self.newsItems = foundStories as! [NewsItem]
	}
	

	
}

