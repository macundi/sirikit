//
//  MasterViewController.swift
//  News-iOS
//
//  Created by Markus Stöbe on 24.08.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import UIKit
import Intents

class MasterViewController: UITableViewController, RssDelegate {

	//******************************************************************************************************************
	//* MARK: - Vars and Outlets
	//******************************************************************************************************************
	var detailViewController: PhoneDetailViewController? = nil
	var objects = [Any]()
	var rssparser: RssParser? = nil

	//******************************************************************************************************************
	//* MARK: - Lifecycle
	//******************************************************************************************************************
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		if let split = self.splitViewController {
		    let controllers = split.viewControllers
		    self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? PhoneDetailViewController
		}
		
		self.rssparser = RssParser.init(delegate: self)
		
		let vocabulary = INVocabulary.shared()
		let userVocabs = NSOrderedSet.init(array: ["Artikelbildern", "Zeige mir Artikelbilder zum Thema", "Suche nach Artikelbildern bei Mac & i","News","Artikel","Texte","Beiträge","Berichte"])
		vocabulary.setVocabularyStrings(userVocabs, of: .photoTag)
		vocabulary.setVocabularyStrings(userVocabs, of: .photoAlbumName)
		
	}

	override func viewWillAppear(_ animated: Bool) {
		self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		self.rssparser?.loadNews()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		INPreferences.requestSiriAuthorization { (siriStatus) in
			if (siriStatus == INSiriAuthorizationStatus.denied) {
				print ("Siri-Access was denied!!!")
			}
		}
	}
	
	//******************************************************************************************************************
	//* MARK: - RssParserDelegate
	//******************************************************************************************************************
	func rssParser(_ parser: RssParser!, didFinishParsingAndFoundNews foundStories: [Any]!) {
		print("I found \(foundStories.count) stories at Mac & i ")
		self.objects = foundStories
		self.tableView.reloadData()
	}

	//******************************************************************************************************************
	//* MARK: - Segues
	//******************************************************************************************************************
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = self.tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row] as! NewsItem
		        let controller = (segue.destination as! UINavigationController).topViewController as! PhoneDetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}

		if segue.identifier == "showSearch" {

			let imageURLs = findNews(forTerms: ["Apple"])

			let controller = segue.destination as! SearchResultViewController
			controller.imageURLs = imageURLs
		}

	}

	func findNews(forTerms terms: [String]) -> [NewsItem] {
		var allResults:Array = [NewsItem]()
		let newsItems:[NewsItem] = self.objects as! [NewsItem]

		for thisTerm in terms {
			let foundItems = newsItems.filter({ (newsItem: NewsItem) -> Bool in
				return newsItem.summary.contains(thisTerm) || newsItem.title.contains(thisTerm)
			})

			if foundItems.count > 0 {
				//add results for this term to array of allresults
				print("we found \(foundItems.count) matching articles")
				allResults += foundItems
			}
		}

		return allResults
	}


	//******************************************************************************************************************
	//* MARK: - Tableview
	//******************************************************************************************************************
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row] as! NewsItem
		cell.textLabel!.text		= object.title
		cell.detailTextLabel!.text	= object.summary
		return cell
	}
}

