//
//  SearchResultViewController.swift
//  News
//
//  Created by Markus Stöbe on 04.09.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController {

	var imageURLs : [NewsItem]?
	@IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

	//******************************************************************************************************************
	//* MARK: - CollectionView-Datasource
	//******************************************************************************************************************
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if let items = imageURLs {
			return items.count
		} else {
			return 0
		}
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {

		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "resultCell", for: indexPath) as! ResultCell

		if let item = self.imageURLs?[indexPath.row] {
			let url       = URL(string:item.imageURL)
			let imageData = NSData(contentsOf: url!)
			let image     = UIImage(data: imageData! as Data)
			cell.imageView.image = image!
		}
		
		return cell
	}

	//******************************************************************************************************************
	//* MARK: - CollectionView-Delegate
	//******************************************************************************************************************
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		collectionView.deselectItem(at: indexPath, animated: true)
	}

	//******************************************************************************************************************
	//* MARK: - FlowLayout-Delegate
	//******************************************************************************************************************
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width  = 100
		let height = 100

		return CGSize.init(width: width, height: height)
	}
}

class ResultCell: UICollectionViewCell {

	@IBOutlet weak var imageView: UIImageView!

}
